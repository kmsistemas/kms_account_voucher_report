# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import pooler
import amount_to_text_es
import wizard
import jasper_reports

class wiz_kms_account_voucher(wizard.interface):
        
    def _print(self, cr, uid, data, context ):
        
        parameters = {}
        
        voucher_obj = pooler.get_pool(cr.dbname).get('account.voucher')
        for voucher in voucher_obj.browse(cr, uid, data['ids'], context=context):
            reads = voucher_obj.read(cr, uid, [voucher.id])
            for record in reads:
                amount = record['amount']
                parameters['IMPORTE_EN_LETRAS'] = amount_to_text_es.amount_to_text(amount, 'es', 'euro')

        return { 
            'parameters': parameters,
            }

    def _action_report_kms_voucher(self, cr, uid, data, context):
        return {
        'target': 'new',
        'type': 'ir.actions.report.xml',
        'report_name': 'report.kms_voucher.jasper',
        'parameters': {}
        } 

    jasper_reports.report_jasper('report.kms_voucher.jasper', 'account.voucher', parser=_print)
    
    states={
        'init':{
            'actions': [_print],
			'result': {'type': 'action', 'action': _action_report_kms_voucher, 'state': 'end', 'rml': 'kms_account_voucher_report/report/Recibo.jrxml'}
        }
    }
wiz_kms_account_voucher('kms_account_voucher_report.printwizard')
