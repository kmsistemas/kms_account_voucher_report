# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'KM Sistemas - KMS Account Voucher Report',
    'version': '1.0',
    'category': 'Reporting',
    'description': """This module replaces the original voucher report layout in RML format
by a Jasper Reports design. When the report is called, it receives a parameter with the
voucher amount converted to its text representation.

**ONLY IN SPANISH!
       """,
    'author': 'KM Sistemas de información, S.L.',
    'depends': ['jasper_reports','account_voucher','nan_account_invoice_sequence'],
    'update_xml': ['kms_account_voucher_report_wizard.xml'],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
